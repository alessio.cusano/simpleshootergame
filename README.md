SimpleShooter Game from Unreal Engine C++ course (GameDev.TV), extended with some features.

The features I implemented:
-Weapons swap.
-Rocket launcher gun with radial damage.
-Weapons ammo.
-AmmoPack dropped by dead enemies.
-Reach the pointing position by pressing E (a base mechanic for cover system).
-Directional damage indicator on the screen, to know from which direction you are getting shot.
-Gradually recover health if you are not receiving damage for a while.   
-Killed enemies counter to know the progress of the game (top-right of the screen).    