// Fill out your copyright notice in the Description page of Project Settings.


#include "Gun.h"
#include "Components/SkeletalMeshComponent.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

// Sets default values
AGun::AGun()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	SetRootComponent(Root);

	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
}

void AGun::PullTrigger() {
	if (Ammo <= 0) {
		UGameplayStatics::SpawnSoundAttached(NoAmmoSound, Mesh, TEXT("MuzzleFlashSocket"));
		return;
	}

	Ammo--;

	UGameplayStatics::SpawnEmitterAttached(MuzzleFlash, Mesh, TEXT("MuzzleFlashSocket"));
	UGameplayStatics::SpawnSoundAttached(MuzzleSound, Mesh, TEXT("MuzzleFlashSocket"));

	FHitResult Hit;
	FVector ShotDirection;

	bool bSuccess = GunTrace(Hit, ShotDirection);

	if (bSuccess) {
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactEffect, Hit.Location, ShotDirection.Rotation());
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ImpactSound, Hit.Location);

		MakeDamage(Hit, ShotDirection);
	}
}


// Called when the game starts or when spawned
void AGun::BeginPlay()
{
	Super::BeginPlay();
	
}


// Called every frame
void AGun::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGun::RefillAmmo(int NewAmmo)
{
	int CurrentAmmo = Ammo;
	Ammo = FGenericPlatformMath::Min(Ammo + NewAmmo, MaxAmmo);
}

int AGun::GetAmmo() const
{
	return Ammo;
}

int AGun::GetMaxAmmo() const
{
	return MaxAmmo;
}

bool AGun::GunTrace(FHitResult& Hit, FVector& ShotDirection)
{
	AController* OwnerController = GetOwnerController();
	if (OwnerController == nullptr) return false;

	FVector Location;
	FRotator Rotation;
	OwnerController->GetPlayerViewPoint(Location, Rotation);
	ShotDirection = -Rotation.Vector();

	FVector End = Location + Rotation.Vector() * MaxRange;
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	return GetWorld()->LineTraceSingleByChannel(Hit, Location, End, ECollisionChannel::ECC_GameTraceChannel1, Params);
}

AController* AGun::GetOwnerController() const
{
	APawn* OwerPawn = Cast<APawn>(GetOwner());
	if (OwerPawn == nullptr) return nullptr;

	return OwerPawn->GetController();
}

// return true if we can add any ammo (and it add the ammo based on the percentage), otherwise if they are already full it return false
bool AGun::AddAmmo(float AmmoPercentage)
{
	// we add at least 1 ammo, if they are not full
	int AmmoAmount = FMath::Max( (int)(AmmoPercentage * MaxAmmo), 1);

	//if the ammo are not full
	if (Ammo < MaxAmmo) {
		// increase the ammo below their max capacity
		Ammo = FMath::Min(Ammo + AmmoAmount, MaxAmmo);
		return true;
	}

	return false;
}

