// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Blueprint/UserWidget.h"
#include "MyUserWidget.h"
#include "SimpleShooter_REPOGameModeBase.h"

#include "ShooterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_REPO_API AShooterPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void GameHasEnded(AActor* EndGameFocus = nullptr, bool bIsWinner = false) override;
	virtual void BeginPlay();
	virtual void ShowDamage(FVector DamagerLocation);

private:
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> HUDClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> LoseScreenClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UUserWidget> WinScreenClass;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class UMyUserWidget> DamageScreenClass;

	UPROPERTY(EditAnywhere)
		float RestartDelay = 5.0f;

	FTimerHandle RestartTimer;

	UPROPERTY(VisibleAnywhere)
		UUserWidget* HUD;

	template <typename WidgetT = UMyUserWidget, typename OwnerT = UObject>
	UMyUserWidget* CreateMyWidget(OwnerT* OwningObject, TSubclassOf<UMyUserWidget> UserWidgetClass = WidgetT::StaticClass(), FName WidgetName = NAME_None);
};
