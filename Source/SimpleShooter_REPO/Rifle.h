// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "Rifle.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_REPO_API ARifle : public AGun
{
	GENERATED_BODY()
	
protected:
	void MakeDamage(FHitResult& Hit, FVector ShotDirection);
};
