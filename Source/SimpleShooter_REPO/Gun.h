// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "GenericPlatform/GenericPlatformMath.h"

#include "Gun.generated.h"

UCLASS()
class SIMPLESHOOTER_REPO_API AGun : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AGun();

	void PullTrigger();

	UFUNCTION(BlueprintCallable)
		bool AddAmmo(float AmmoPercentage);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	virtual void MakeDamage(FHitResult &Hit, FVector ShotDirection) PURE_VIRTUAL(AGun::MakeDamage,);

	AController* GetOwnerController() const;

	UPROPERTY(EditAnywhere)
		float MaxRange = 1000;

	UPROPERTY(EditAnywhere)
		float Damage = 10;

	UPROPERTY(EditAnywhere)
		int Ammo = 15;

	UPROPERTY(EditAnywhere)
		int MaxAmmo = 20;

	bool GunTrace(FHitResult& Hit, FVector& ShotDirection);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual void RefillAmmo(int NewAmmo);

	int GetAmmo() const;
	int GetMaxAmmo() const;

private:
	UPROPERTY(VisibleAnywhere)
		USceneComponent* Root;

	UPROPERTY(VisibleAnywhere)
		USkeletalMeshComponent* Mesh;

	UPROPERTY(EditAnywhere)
		UParticleSystem* MuzzleFlash;

	UPROPERTY(EditAnywhere)
		USoundBase* MuzzleSound;

	UPROPERTY(EditAnywhere)
		UParticleSystem* ImpactEffect;

	UPROPERTY(EditAnywhere)
		USoundBase* ImpactSound;

	UPROPERTY(EditAnywhere)
		USoundBase* NoAmmoSound;
};
