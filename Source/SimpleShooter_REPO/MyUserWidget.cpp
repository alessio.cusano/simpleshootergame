// Fill out your copyright notice in the Description page of Project Settings.


#include "MyUserWidget.h"

// we store the location of the damager actor so we can update the damage indicator while you move to keep it in the right direction.
void UMyUserWidget::SetDamagerLocation(FVector DLocation)
{
	this->DamagerLocation = DLocation;
	
}

// calculate the angle from the damager actor using the damager location and the direction of the player.
float UMyUserWidget::CalculateDamagerAngle()
{
	AController* OwnerController = GetOwningPlayerPawn()->GetController();	
	AShooterPlayerController* ShooterController = Cast<AShooterPlayerController>(OwnerController);

	// we don't check if OwnerController nor ShooterController is null. In that case the game will crash, but probably something bad happened, so let it crash and find out what's the problem.
	
	FVector Location;
	FRotator Rotation;
	OwnerController->GetPlayerViewPoint(Location, Rotation);
	FVector Direction = -Rotation.Vector();

	FVector End = Location + Rotation.Vector() * 100000;

	Angle = GetDamagerAngle(DamagerLocation - Location, End - Location);

	return Angle;
}

// given two vectors, this will return the angle between them using the dot product.
float UMyUserWidget::GetDamagerAngle(FVector v1, FVector v2) {

	FVector _v1 = FVector(v1.X, v1.Y, 0);
	FVector _v2 = FVector(v2.X, v2.Y, 0);

	_v1.Normalize();
	_v2.Normalize();

	float angle = acos(FVector::DotProduct(_v1, _v2));
	FVector cross = FVector::CrossProduct(_v1, _v2);
	if (FVector::DotProduct(FVector(1, 1, 1), cross) < 0) { // Or > 0
		angle = -angle;
	}
	angle = -FMath::RadiansToDegrees(angle);

	return angle;
}