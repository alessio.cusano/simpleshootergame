// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterPlayerController.h"

void AShooterPlayerController::GameHasEnded(AActor* EndGameFocus, bool bIsWinner) {
	Super::GameHasEnded(EndGameFocus, bIsWinner);

	if (HUD != nullptr) HUD->RemoveFromViewport();
	if (bIsWinner) {
		UUserWidget* WinScreen = CreateWidget(this, WinScreenClass);
		if (WinScreen != nullptr) {
			WinScreen->AddToViewport();
		}
	}
	else {
		UUserWidget* LoseScreen = CreateWidget(this, LoseScreenClass);
		if (LoseScreen != nullptr) {
			LoseScreen->AddToViewport();
		}
	}
	GetWorldTimerManager().SetTimer(RestartTimer, this, &APlayerController::RestartLevel, RestartDelay);
}

void AShooterPlayerController::BeginPlay()
{
	Super::BeginPlay();

	HUD = CreateWidget(this, HUDClass);
	if (HUD != nullptr) {
		HUD->AddToViewport();
	}

	ASimpleShooter_REPOGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooter_REPOGameModeBase>();
	if (GameMode != nullptr) {
		GameMode->CalculateEnemiesTokill();
	}
}

// spawn the new damage indicator and set the damager location (the rest of the logic is in the blueprint of the widget).
void AShooterPlayerController::ShowDamage(FVector DamagerLocation)
{
	UMyUserWidget* Widget = CreateMyWidget(this, DamageScreenClass);
	if (Widget != nullptr) {
		Widget->AddToViewport();
		Widget->SetDamagerLocation(DamagerLocation);
	}
}

// "interface" to spawn our custom widget class
template<typename WidgetT, typename OwnerT>
UMyUserWidget* AShooterPlayerController::CreateMyWidget(OwnerT* OwningObject, TSubclassOf<UMyUserWidget> UserWidgetClass, FName WidgetName)
{
	return Cast< UMyUserWidget >( CreateWidget(OwningObject, TSubclassOf<class UUserWidget>(UserWidgetClass), WidgetName) );
}
