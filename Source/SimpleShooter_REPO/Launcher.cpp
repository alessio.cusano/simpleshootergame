// Fill out your copyright notice in the Description page of Project Settings.


#include "Launcher.h"

void ALauncher::MakeDamage(FHitResult& Hit, FVector ShotDirection) {
	FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
	UGameplayStatics::ApplyRadialDamage(GetWorld(), Damage, Hit.Location, DamageRadius, UDamageType::StaticClass(), TArray<AActor*>(), this, GetOwnerController());
}