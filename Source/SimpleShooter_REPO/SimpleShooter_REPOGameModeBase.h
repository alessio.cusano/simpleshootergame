// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "ShooterAIController.h"
#include "EngineUtils.h"

#include "SimpleShooter_REPOGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_REPO_API ASimpleShooter_REPOGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	virtual void PawnKilled(APawn* PawnKilled);

protected:
	int EnemiesToKill = 0;
	int EnemiesKilled = 0;

public:
	UFUNCTION(BlueprintPure)
		int GetEnemiesToKill() const;

	UFUNCTION(BlueprintPure)
		int GetEnemiesKilled() const;

	void IncrementEnemiesKilled();
	void CalculateEnemiesTokill();
};
