// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include <SimpleShooter_REPO\ShooterPlayerController.h>

#include "MyUserWidget.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_REPO_API UMyUserWidget : public UUserWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Damager")
		FVector DamagerLocation;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Damager")
		float Angle;

public:
	void SetDamagerLocation(FVector DLocation);

protected:
	UFUNCTION(BlueprintCallable, Category = "Damager")
		float CalculateDamagerAngle();

private:
	float GetDamagerAngle(FVector v1, FVector v2);
};
