// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Gun.h"
#include "Components/CapsuleComponent.h"
#include "SimpleShooter_REPOGameModeBase.h"
#include "Containers/Array.h"
#include "GameFramework/PlayerController.h"
#include "ShooterPlayerController.h"

#include "GenericPlatform/GenericPlatformMath.h"

#include "ShooterCharacter.generated.h"

UCLASS()
class SIMPLESHOOTER_REPO_API AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AShooterCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	UFUNCTION(BlueprintPure)
		bool IsDead() const;

	UFUNCTION(BlueprintPure)
		float GetHealthPercent() const;

	UFUNCTION(BlueprintPure)
		int GetCurrentAmmo() const;

	UFUNCTION(BlueprintPure)
		int GetCurrentMaxAmmo() const;

	UFUNCTION(BlueprintCallable)
		bool IncreaseHealth(float Tick, float Speed);

	UFUNCTION(BlueprintCallable)
		bool IsFullHealth() const;

	UFUNCTION(BlueprintCallable)
		float GetHealth() const;

	UFUNCTION(BlueprintCallable)
		bool AddAmmo(float AmmoPercentage);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser);
	
	void Shoot();

	virtual void SelectGun(int newIdx, int prevIdx = -1);
	virtual void NextGun();
	virtual void PrevGun();
	virtual void ReachCover();

	void ShowDamageIndicator(FVector DamagerLocation);

private:
	void MoveForward(float AxisValue);
	void MoveRight(float AxisValue);
	void LookUpRate(float AxisValue);
	void LookRightRate(float AxisValue);
	void SelectGun1();
	void SelectGun2();
	bool CameraTrace(FHitResult& Hit, FVector& Direction);
	void SpawnAmmoPack();


	UPROPERTY(EditAnywhere)
		float RotationRate = 10;

	UPROPERTY(EditDefaultsOnly)
		float MaxHealth = 100;

	UPROPERTY(VisibleAnywhere)
		float Health;

	UPROPERTY(EditAnywhere)
		float MaxCoverRange = 200;

	UPROPERTY(EditAnywhere)
		float DeltaCover = 100.0f;

	TArray<AGun*> Guns;

	UPROPERTY(EditDefaultsOnly)
		TArray<TSubclassOf<AGun>> GunsClasses;

	int CurrentGunIdx = 0;

	UPROPERTY(VisibleAnywhere)
		bool ReachingCover = false;
	
	UPROPERTY(VisibleAnywhere)
		FVector CoverPoint;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<AActor> AmmoPackClass;

	UPROPERTY(EditAnywhere)
		float DeltaYAmmoPack = 100.0f;

	UPROPERTY(EditAnywhere)
		float AmmoProbabilities = 0.3f;
};
