// Fill out your copyright notice in the Description page of Project Settings.


#include "Rifle.h"

void ARifle::MakeDamage(FHitResult& Hit, FVector ShotDirection) {
	AActor* HitActor = Hit.GetActor();
	if (HitActor != nullptr) {
		FPointDamageEvent DamageEvent(Damage, Hit, ShotDirection, nullptr);
		AController* OwnerController = GetOwnerController();
		HitActor->TakeDamage(Damage, DamageEvent, OwnerController, this);
	}
}