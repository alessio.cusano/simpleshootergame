// Copyright Epic Games, Inc. All Rights Reserved.


#include "SimpleShooter_REPOGameModeBase.h"

void ASimpleShooter_REPOGameModeBase::PawnKilled(APawn* PawnKilled) {
	
}

int ASimpleShooter_REPOGameModeBase::GetEnemiesToKill() const
{
	return EnemiesToKill;
}

int ASimpleShooter_REPOGameModeBase::GetEnemiesKilled() const
{
	return EnemiesKilled;
}

void ASimpleShooter_REPOGameModeBase::IncrementEnemiesKilled()
{
	EnemiesKilled++;
}

void ASimpleShooter_REPOGameModeBase::CalculateEnemiesTokill()
{
	for (AShooterAIController* Controller : TActorRange<AShooterAIController>(GetWorld())) {
		EnemiesToKill++;
	}
}
