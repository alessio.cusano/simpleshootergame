// Fill out your copyright notice in the Description page of Project Settings.


#include "ShooterCharacter.h"

// Sets default values
AShooterCharacter::AShooterCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

bool AShooterCharacter::IsDead() const {
	return Health <= 0;
}

float AShooterCharacter::GetHealthPercent() const
{
	return Health / MaxHealth;
}

int AShooterCharacter::GetCurrentAmmo() const
{
	return Guns[CurrentGunIdx]->GetAmmo();
}

int AShooterCharacter::GetCurrentMaxAmmo() const
{
	return Guns[CurrentGunIdx]->GetMaxAmmo();
}

bool AShooterCharacter::IncreaseHealth(float Tick, float Speed)
{
	// cap the health to the max health
	Health = FMath::Min(Health + Speed * Tick, MaxHealth);
	return Health < MaxHealth;
}

bool AShooterCharacter::IsFullHealth() const
{
	return Health >= MaxHealth;
}

float AShooterCharacter::GetHealth() const
{
	return Health;
}

// return true if we can add any ammo to any gun (and it add the ammo to the gun based on the percentage), otherwise it return false if every gun is full
bool AShooterCharacter::AddAmmo(float AmmoPercentage)
{
	bool result = false;
	bool aux = false;

	for (AGun* Gun : Guns) {
		// if you have more than one gun and the first one can add a few ammo, then "result" will be true and if you write "result = result || Gun->AddAmmo(AmmoPercentage)" then the optimizer
		// won't call AddAmmo on the other guns, because "true || everything" will be always true, but we need this call otherwise the other guns won't increase their ammo
		aux = Gun->AddAmmo(AmmoPercentage); 

		result = result || aux;
	}

	return result;
}

// Called when the game starts or when spawned
void AShooterCharacter::BeginPlay()
{
	Super::BeginPlay();
	
	Health = MaxHealth;

	// for each type of gun
	for (int i = 0; i < GunsClasses.Num(); i++) {

		// spawn it and store it
		Guns.Insert(GetWorld()->SpawnActor<AGun>(GunsClasses[i]), i);

		// hide the old weapon
		GetMesh()->HideBoneByName(TEXT("weapon_r"), EPhysBodyOp::PBO_None);

		// attach it to the weapon socket of the player
		Guns[i]->AttachToComponent(GetMesh(), FAttachmentTransformRules::KeepRelativeTransform, TEXT("WeaponSocket"));

		// set the player as the owner
		Guns[i]->SetOwner(this);

		// show only the selected gun
		if (i != CurrentGunIdx) Guns[i]->SetActorHiddenInGame(true);
	}
}

// Called every frame
void AShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// if you pressed E then you will be start to reach the cover
	if (ReachingCover) {
		// so we add movement toward it
		AddMovementInput(CoverPoint - GetActorLocation());

		// and we stop if we reach a "DeltaCover" distance
		ReachingCover = (CoverPoint - GetActorLocation()).Size() > DeltaCover;
	}
}

// Called to bind functionality to input
void AShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis(TEXT("MoveForward"), this, &AShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis(TEXT("MoveRight"), this, &AShooterCharacter::MoveRight);
	PlayerInputComponent->BindAxis(TEXT("LookUp"), this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis(TEXT("LookRight"), this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis(TEXT("LookUpRate"), this, &AShooterCharacter::LookUpRate);
	PlayerInputComponent->BindAxis(TEXT("LookRightRate"), this, &AShooterCharacter::LookRightRate);
	PlayerInputComponent->BindAction(TEXT("Jump"), EInputEvent::IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction(TEXT("Shoot"), EInputEvent::IE_Pressed, this, &AShooterCharacter::Shoot);
	PlayerInputComponent->BindAction(TEXT("Gun1"), EInputEvent::IE_Pressed, this, &AShooterCharacter::SelectGun1);
	PlayerInputComponent->BindAction(TEXT("Gun2"), EInputEvent::IE_Pressed, this, &AShooterCharacter::SelectGun2);
	PlayerInputComponent->BindAction(TEXT("PrevGun"), EInputEvent::IE_Pressed, this, &AShooterCharacter::PrevGun);
	PlayerInputComponent->BindAction(TEXT("NextGun"), EInputEvent::IE_Pressed, this, &AShooterCharacter::NextGun);
	PlayerInputComponent->BindAction(TEXT("ReachCover"), EInputEvent::IE_Pressed, this, &AShooterCharacter::ReachCover);
}

float AShooterCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) {
	float DamageToApply = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	DamageToApply = FMath::Min(Health, DamageToApply);
	Health -= DamageToApply;

	if (IsDead()) {
		ASimpleShooter_REPOGameModeBase* GameMode = GetWorld()->GetAuthGameMode<ASimpleShooter_REPOGameModeBase>();
		if (GameMode != nullptr) {
			GameMode->PawnKilled(this);
		}

		SpawnAmmoPack();

		DetachFromControllerPendingDestroy();
		GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	}

	ShowDamageIndicator(DamageCauser->GetActorLocation());

	return DamageToApply;
}


void AShooterCharacter::MoveForward(float AxisValue)
{
	AddMovementInput(GetActorForwardVector() * AxisValue);
}

void AShooterCharacter::MoveRight(float AxisValue)
{
	AddMovementInput(GetActorRightVector() * AxisValue);
}

void AShooterCharacter::LookUpRate(float AxisValue)
{
	AddControllerPitchInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::LookRightRate(float AxisValue)
{
	AddControllerYawInput(AxisValue * RotationRate * GetWorld()->GetDeltaSeconds());
}

void AShooterCharacter::SelectGun1()
{
	SelectGun(0);
}

void AShooterCharacter::SelectGun2()
{
	SelectGun(1);
}

// it make a trace ih the camera direction and it return true if it detect a collision, with the data about it into Hit and Direction
bool AShooterCharacter::CameraTrace(FHitResult& Hit, FVector& Direction)
{
	AController* OwnerController = GetController();
	if (OwnerController == nullptr) return false;

	FVector ActorLocation = OwnerController->GetPawn()->GetActorLocation();
	FVector Location;
	FRotator Rotation;
	OwnerController->GetPlayerViewPoint(Location, Rotation);
	Direction = -Rotation.Vector();

	FVector End = Location + Rotation.Vector() * 100000;

	// we ignore ourself
	FCollisionQueryParams Params;
	Params.AddIgnoredActor(this);
	Params.AddIgnoredActor(GetOwner());

	// the ECC_GameTraceChannel2 channel represent the cover.
	bool bSuccess = GetWorld()->LineTraceSingleByChannel (Hit, Location, End, ECollisionChannel::ECC_GameTraceChannel2, Params);
	return bSuccess && (Hit.ImpactPoint - ActorLocation).Size() < MaxCoverRange;
}

void AShooterCharacter::SpawnAmmoPack()
{
	// get a random value from 0 to 1 to compare with the spawn probability
	float val = FMath::FRand();

	// if it is less than the probabilities, then spawn the ammo
	if (val < AmmoProbabilities) {
		AActor* AmmoPack = GetWorld()->SpawnActor<AActor>(AmmoPackClass);
		if (AmmoPack != nullptr) {
			// and set it to the right position
			AmmoPack->SetActorLocation(GetActorLocation() - FVector(0.f, DeltaYAmmoPack, 0.f));
		}
	}
}

void AShooterCharacter::ShowDamageIndicator(FVector DamagerLocation)
{
	AController* OwnerController = GetController();
	if (OwnerController == nullptr) return;
	
	AShooterPlayerController* ShooterController = Cast<AShooterPlayerController>(OwnerController);
	if (ShooterController == nullptr) return;

	ShooterController->ShowDamage(DamagerLocation);
}

void AShooterCharacter::Shoot() {
	// check if the player can shoot and the gun is valid
	if(!IsDead() && Guns.IsValidIndex(CurrentGunIdx))
		Guns[CurrentGunIdx]->PullTrigger();
}

// select the new gun and hide the previous one
void AShooterCharacter::SelectGun(int newIdx, int prevIdx)
{
	if (prevIdx == -1) prevIdx = CurrentGunIdx;

	if (newIdx >= 0 && newIdx < Guns.Num() && newIdx != prevIdx) {
		Guns[prevIdx]->SetActorHiddenInGame(true);
		Guns[newIdx]->SetActorHiddenInGame(false);
		CurrentGunIdx = newIdx;
	}
}

void AShooterCharacter::NextGun()
{
	int prevGun = CurrentGunIdx;
	CurrentGunIdx = (++CurrentGunIdx) % Guns.Num();
	SelectGun(CurrentGunIdx, prevGun);
}

void AShooterCharacter::PrevGun()
{
	int prevGun = CurrentGunIdx;
	CurrentGunIdx--;

	if (CurrentGunIdx < 0) {
		CurrentGunIdx = Guns.Num() - 1;
	}

	SelectGun(CurrentGunIdx, prevGun);
}

void AShooterCharacter::ReachCover()
{
	FHitResult Hit;
	FVector Direction;

	bool bValidCover = CameraTrace(Hit, Direction);

	// If you are already reaching the cover and you press again the button, then stop and not reach it anymore.
	if (ReachingCover) {
		ReachingCover = false;
	}
	// Otherwise, if the cover is valid, then reach it.
	else if (bValidCover) {
		ReachingCover = true;
		CoverPoint = Hit.ImpactPoint;// -Hit.ImpactNormal * DeltaCover;
	}
}
