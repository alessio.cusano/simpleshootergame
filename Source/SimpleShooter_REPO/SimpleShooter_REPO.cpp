// Copyright Epic Games, Inc. All Rights Reserved.

#include "SimpleShooter_REPO.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SimpleShooter_REPO, "SimpleShooter_REPO" );
