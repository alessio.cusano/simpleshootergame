// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Gun.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/DamageType.h"

#include "Launcher.generated.h"

/**
 * 
 */
UCLASS()
class SIMPLESHOOTER_REPO_API ALauncher : public AGun
{
	GENERATED_BODY()

protected:
	void MakeDamage(FHitResult& Hit, FVector ShotDirection);

	UPROPERTY(EditAnywhere, Category="Gun")
		float DamageRadius = 50;

};
